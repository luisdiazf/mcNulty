# Two stage example Virtual Machine file
# usb port needs to be set in initInterfaces
# Nadya Peek Dec 2014

# MTM 2016 - FabAcademy 2016
# Supernode FabLab Leon (Leon, Deusto, Universidad Europea, Madrid CEU)
# Creative Coding : Luis Diaz, Borja Lanza, Marta Verde
# Paco Gonzalez, Jose Real, Victoria Retana, Maria Santisteban

#------IMPORTS-------

#------Pygestalt-------
from pygestalt import nodes
from pygestalt import interfaces
from pygestalt import machines
from pygestalt import functions
from pygestalt.machines import elements
from pygestalt.machines import kinematics
from pygestalt.machines import state
from pygestalt.utilities import notice
from pygestalt.publish import rpc       #remote procedure call dispatcher

#------Other Libraries-------
import time
import io
import sys
import wx
import wx.lib.platebtn as platebtn
import wx.grid as gridlib
import csv
import threading
import OSC
from StringIO import StringIO



#------Other Python scripts-------
import dataGrid

#Variables globales
circcoords = []
global b_osc
b_osc = False
(w, h) = (2, 2)

#------VIRTUAL MACHINE------
class virtualMachine(machines.virtualMachine):

    def initInterfaces(self):
        if self.providedInterface: self.fabnet = self.providedInterface         #providedInterface is defined in the virtualMachine class.
        #else: self.fabnet = interfaces.gestaltInterface('FABNET', interfaces.serialInterface(baudRate = 115200, interfaceType = 'ftdi', portName = 'COM6')) # Uncomment for Windows
        else: self.fabnet = interfaces.gestaltInterface('FABNET', interfaces.serialInterface(baudRate = 115200, interfaceType = 'ftdi', portName = '/dev/tty.usbserial-FTXW9XTL'))#Route for Mac (serial may change)

    def initControllers(self):
        self.yAxisNode = nodes.networkedGestaltNode('Y Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.zlAxisNode = nodes.networkedGestaltNode('ZL Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.zrAxisNode = nodes.networkedGestaltNode('ZR Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.wAxisNode = nodes.networkedGestaltNode('W Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.yzzwNode = nodes.compoundNode(self.yAxisNode, self.zlAxisNode, self.zrAxisNode, self.wAxisNode)

    def initCoordinates(self):
        self.position = state.coordinate(['mm', 'mm', 'mm', 'rev'])

    def initKinematics(self):
        self.yAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(False)])
        self.zlAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(False)])
        self.zrAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(False)])
        self.wAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8),  elements.invert.forward(False)])

        self.stageKinematics = kinematics.direct(4)     #direct drive on all axes

    def initFunctions(self):
        self.move = functions.move(virtualMachine = self, virtualNode = self.yzzwNode, axes = [self.yAxis, self.zlAxis, self.zrAxis, self.wAxis], kinematics = self.stageKinematics, machinePosition = self.position,planner = 'null')
        self.jog = functions.jog(self.move)     #an incremental wrapper for the move function
        pass

    def initLast(self):
        #self.machineControl.setMotorCurrents(aCurrent = 0.8, bCurrent = 0.8, cCurrent = 0.8)
        #self.xNode.setVelocityRequest(0)       #clear velocity on nodes. Eventually this will be put in the motion planner on initialization to match state.
        pass

    def publish(self):
        #self.publisher.addNodes(self.machineControl)
        pass

    def getPosition(self):
        return {'position':self.position.future()}

    def setPosition(self, position  = [None]):
        self.position.future.set(position)

    def setSpindleSpeed(self, speedFraction):
        #self.machineControl.pwmRequest(speedFraction)
        pass



#---- OSC

def initOSC(self):
    receive_address = '192.168.0.11', 8000

    # OSC Server. there are three different types of server.
    s = OSC.OSCServer(receive_address) # basic

    # this registers a 'default' handler (for unmatched messages),
    # an /'error' handler, an '/info' handler.
    # And, if the client supports it, a '/subscribe' & '/unsubscribe' handler
    s.addDefaultHandlers()

    s.addMsgHandler("MTM/btn_help/", button_handler) # adding our function
    s.addMsgHandler("MTM/btn_move/", button_handler) # adding our function

# Start OSCServer
    print "\nStarting OSCServer."
    global st
    st = threading.Thread( target = s.serve_forever )
    st.start()

    '''
    if b_osc != True:
        print "\nStarting OSCServer."
        global st
        st = threading.Thread( target = s.serve_forever )
        st.start()
        b_osc = True
    else:
        print "\nClosing OSCServer."
        st.close()
        b_osc = False
    '''
#Funtions

def openFile():
    fname = raw_input("Enter filename: ")
    if len(fname) == 0:
        fname = "test2.csv"
    try:
        fh = open(fname,'r')
    except:
        print "Error: File (", fname, ") cannot be opened. \nOpening test file instead."
        fname = "test2.csv"
        fh = open(fname,'r')
        #exit()

    #circcoords = []
    for line in fh.readlines():
        xy = line.split(',')
        coord = []
        for num in xy:
            try:
                nr = int(num)
                coord.append(nr)
            except:
                pass
        circcoords.append(coord)
    circcoords.append([0,0,0,0])
    fh.close()

    #move_Motors(circcoords)
    print circcoords
    print ("Reading done!")

def openFileM(object):

    fh = open(object,'r')
    #circcoords = []
    for line in fh.readlines():
        xy = line.split(',')
        coord = []
        for num in xy:
            try:
                nr = int(num)
                coord.append(nr)
            except:
                pass
        circcoords.append(coord)
    circcoords.append([0,0,0,0])
    fh.close()

    #move_Motors(circcoords)
    print circcoords
    print ("Reading done!")

def move_Motors(moves):

    for element in moves:
        print element


    print ("Cut Done")
    '''
    for move in moves:
        stages.move(move, 0)
        status = stages.yAxisNode.spinStatusRequest()
        # This checks to see if the move is done.
        while status['stepsRemaining'] > 0:
            time.sleep(0.001)
            status = stages.yAxisNode.spinStatusRequest()
    '''


def move_step(a,b,c,d):
    print (a,b)
    #stages.jog([a, b, c, d])

def button_handler(addr, tags, args, source):

    if (args[0]==1.0 and addr == "/MTM/btn_move"):
        #print "Bang"
        #openFile()
        move_Motors(circcoords)
    elif (args[0]==1.0 and addr == "/MTM/btn_help"):
        print "Bang"
        frame = helpFrame()
        frame.Show()
    else:
        pass

def null_handler(addr, tags, args, source):
    pass


def machineSetup():
    # The persistence file remembers the node you set. It'll generate the first time you run the
    # file. If you are hooking up a new node, delete the previous persistence file.
    stages = virtualMachine(persistenceFile = "test.vmp")

    # You can load a new program onto the nodes if you are so inclined. This is currently set to
    # the path to the 086-005 repository on Nadya's machine.
    #stages.xyNode.loadProgram('../../../086-005/086-005a.hex')


    # This is a widget for setting the potentiometer to set the motor current limit on the nodes.
    # The A4982 has max 2A of current, running the widget will interactively help you set.
    #stages.xyNode.setMotorCurrent(0.7)

    # This is for how fast the motors for move
    stages.yzzwNode.setVelocityRequest(1)

class MyPanel(wx.Panel):
    def __init__(self, parent):
        super(MyPanel, self).__init__(parent)

        # Sizer to control button layout
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        vsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer.Add(sizer)

        # Button with a bitmap
        button = wx.Button(self, label="Open File")
        bitmap = wx.Bitmap('imgs/bopen.png')
        button.SetBitmap(bitmap)
        sizer.Add(button)

        # Button with a bitmap
        buttoncut = wx.Button(self, label="Cut")
        bitmap = wx.Bitmap('imgs/bcut.png')
        buttoncut.SetBitmap(bitmap)
        sizer.Add(buttoncut)

        # Button with a bitmap
        buttonabout = wx.Button(self, label="About")
        bitmap = wx.Bitmap('imgs/babout.png')
        buttonabout.SetBitmap(bitmap)
        sizer.Add(buttonabout)

        # Button with a bitmap
        buttonhelp = wx.Button(self, label="Help")
        bitmap = wx.Bitmap('imgs/bhelp.png')
        buttonhelp.SetBitmap(bitmap)
        sizer.Add(buttonhelp)

        # Button with a bitmap
        buttonstep = wx.Button(self, -1, 'Step', (220, 120))
        sizer.Add(buttonstep)

        self.SetSizer(sizer)
        self.Bind(wx.EVT_BUTTON, self.OnButton)

        wx.StaticText(self, -1, 'Y axis step:', (20, 70))
        wx.StaticText(self, -1, 'Z axis step:', (20, 120))
        self.sc1 = wx.SpinCtrl(self, -1, str(w), (100, 65), (60, -1), min=-20, max=20)
        self.sc2 = wx.SpinCtrl(self, -1, str(h), (100, 115), (60, -1), min=-20, max=20)

    def OnButton(self,event):
        global frame
        buttonLabel =  event.GetEventObject().GetLabel()
        if buttonLabel == "Open File":
            print ("Please enter the name of the file to open")
            openFile()
            self.GetParent().SetStatusText('Opening File')

        elif buttonLabel == "Cut":
            print ("Cutting Coords")
            move_Motors(circcoords)
            self.GetParent().SetStatusText('Cutting File ')
        elif buttonLabel == "Help":
            print ("Help")
            frame = helpFrame()
            frame.Show()
            self.GetParent().SetStatusText('How to use the foam cutter')
        elif buttonLabel == "About":
            print ("About")
            self.GetParent().SetStatusText('Know about us')
            self.GetParent().OnAbout(wx.ID_ABOUT)
        elif buttonLabel == "Step":
            self.GetParent().SetStatusText('Step')

            move_step(self.sc2.GetValue(),self.sc1.GetValue(),self.sc1.GetValue(),0)


class MyFrame(wx.Frame):
    def __init__(self, parent, title=""):
        super(MyFrame, self).__init__(parent, title=title, size=(500,200))

        # Set the panel
        self.panel = MyPanel(self)
        self.SetBackgroundColour( wx.Colour( 0, 141, 254 ) )
        # Setup Menus
        menubar = wx.MenuBar()
        filemenu = wx.Menu()
        filemenu.Append(wx.ID_OPEN)
        filemenu.Append(wx.ID_CUT)
        menubar.Append(filemenu, "File")
        configmenu = wx.Menu()
        configmenu.Append(wx.ID_SETUP, "OSC On")
        #configmenu.Append(wx.ID_SETUP, "OSC Off")
        configmenu.Append(wx.ID_ABOUT, "About Mcnulty")
        menubar.Append(configmenu, "Setup")
        self.SetMenuBar(menubar)

        # Setup StatusBar
        self.CreateStatusBar()
        self.PushStatusText("Run the Machine configuration")


    # Event Handlers
        self.Bind(wx.EVT_MENU, self.OnAbout, id=wx.ID_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnCut, id=wx.ID_CUT)
        self.Bind(wx.EVT_MENU, self.OnOpen, id=wx.ID_OPEN)
        self.Bind(wx.EVT_MENU, self.OnOSC, id=wx.ID_SETUP)


    def OnOpen(self, event):
        dlg = wx.FileDialog(self, "Open CSV File", wildcard="*.csv")
        result = dlg.ShowModal()
        if result == wx.ID_OK:
            openFileM(dlg.Path)
        dlg.Destroy()
        #self.SetStatusText("Open file: %s" % self._file)

    def OnCut(self, event):
        move_Motors(circcoords)
        self.SetStatusText("Cutting File")

    def OnOSC(self, event):

        initOSC(self)
        self.SetStatusText("OSC Setup")


    #About menu
    def OnAbout(self, event):
        """Show the about dialog"""
        info = wx.AboutDialogInfo()

        # Make a template for the description
        desc = ["\nFoam Cutter 2016\n",
                "Platform Info: (%s,%s)",
                "License: Public Domain"]
        desc = "\n".join(desc)



        # Populate with information
        info.SetName("Foldable Mcnulty Hot Wire Cutter")
        info.SetVersion("FA16 - 0.6")
        info.SetIcon(wx.Icon('imgs/mcnulty_ico.png'))
        info.SetCopyright("Copyleft : Luis Diaz, Borja Lanza, Marta Verde\nPaco Gonzalez, Maria Santisteban, Jose Real, Victoria Retana")
        info.SetWebSite("http://archive.fabacademy.org/archives/2016/fablabmadridceu/mtm/mtm2016.html")
        info.SetDescription("Foam Cutter GUI system")

        # Create and show the dialog
        wx.AboutBox(info)



class helpFrame(wx.Frame):
    """"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        wx.Frame.__init__(self, None, title="Help")
        self.SetBackgroundColour( wx.Colour( 158, 191, 254 ) )
        panel = wx.Panel(self)
        txt = wx.StaticText(panel, label="Welcome to McNulty Help! \nFirst be sure that all the cables are connected\n1 - Open a CSV File\n2 - Cut File")


class MyApp(wx.App):
    def OnInit(self):
        #machineSetup()

        self.frame = MyFrame(None, title="McNulty Foldable Hot Wire Foam Machine")
        self.frame.Show();
        return True


if __name__ == "__main__":
    app = MyApp(False)
    app.MainLoop()