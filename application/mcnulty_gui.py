import time
import io
import sys
import wx
import wx.lib.platebtn as platebtn
import wx.grid as gridlib
import wx.html
import threading
from StringIO import StringIO
import matplotlib as mpl
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.figure import Figure

import mcnulty3 as mcn


import mcnulty_gui as GUI

import pdb

#------Other Python scripts-------


#matplotlib config
#mpl.rcParams['axes.facecolor'] = '111111' # Graph background color
mpl.rcParams['savefig.transparent'] = True # Graph background color

#Variables globales

(w, h) = (2, 2)



class MyPanel(wx.Panel):
    def __init__(self, parent):
        super(MyPanel, self).__init__(parent)

        # Sizer to control button layout
        sizer = wx.BoxSizer(wx.VERTICAL)
        top_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        mid_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        bot_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(top_h_sizer)
        sizer.Add(mid_h_sizer)
        sizer.Add(bot_h_sizer)

        #Top Sizer
        # Button with a bitmap
        button = wx.Button(self, label="Open csv File")
        bitmap = wx.Bitmap('imgs/bopen.png')
        button.SetBitmap(bitmap)
        button.ToolTipString = "Open and import a comma separated values file for cut"
        top_h_sizer.Add(button)

        # Button with a bitmap
        buttondat = wx.Button(self, label="Open dat File")
        bitmap = wx.Bitmap('imgs/bopen.png')
        buttondat.SetBitmap(bitmap)
        buttondat.ToolTipString = "Open and import a dat file (Airfol)"
        top_h_sizer.Add(buttondat)

        # Button with a bitmap
        buttoncut = wx.Button(self, label="Cut")
        bitmap = wx.Bitmap('imgs/bcut.png')
        buttoncut.SetBitmap(bitmap)
        buttoncut.ToolTipString = "The machine will start cutting the coordinates loaded"
        top_h_sizer.Add(buttoncut)

        # Button with a bitmap
        buttonplot = wx.Button(self, label="Plot")
        bitmap = wx.Bitmap('imgs/bplot.png')
        buttonplot.SetBitmap(bitmap)
        buttonplot.ToolTipString = "Plot into a graph the values of a dat file."
        top_h_sizer.Add(buttonplot)

        # Button with a bitmap
        buttonabout = wx.Button(self, label="About")
        bitmap = wx.Bitmap('imgs/babout.png')
        buttonabout.SetBitmap(bitmap)
        buttonabout.ToolTipString = "Open an window with info about the project"
        top_h_sizer.Add(buttonabout)

        # Button with a bitmap
        buttonhelp = wx.Button(self, label="Help")
        bitmap = wx.Bitmap('imgs/bhelp.png')
        buttonhelp.SetBitmap(bitmap)
        buttonhelp.ToolTipString = "Open an import a comma separated values file for cut"
        top_h_sizer.Add(buttonhelp)

        #Medium Sizer
        # Graph
        self.figure = Figure(figsize=(10,6), facecolor='#008DFF')
        self.axes = self.figure.add_subplot(111,aspect=1.0)
        self.figurecanvas = FigureCanvas(self, -1, self.figure)
        self.axes.autoscale_view(tight=None, scalex=False, scaley=False)
        mid_h_sizer.Add(self.figurecanvas)

        #Bottom Sizer

        buttonscale = wx.Button(self, label="Scale")
        bitmap = wx.Bitmap('imgs/bscale.png')
        buttonscale.SetBitmap(bitmap)
        bot_h_sizer.Add(buttonscale)

        scaleText = wx.TextCtrl(self, value="10", style=wx.TE_PROCESS_ENTER)
        bot_h_sizer.Add(scaleText)
        self.Bind(wx.EVT_TEXT_ENTER,self.OnEnterPressed)

        # Button with a bitmap
        buttonstep = wx.Button(self, -1, 'Move Axis', (220, 120))
        bitmap = wx.Bitmap('imgs/baxis.png')
        buttonstep.SetBitmap(bitmap)
        bot_h_sizer.Add(buttonstep)

        self.SetSizer(sizer)
        self.Bind(wx.EVT_BUTTON, self.OnButton)

        ytext=wx.StaticText(self, -1, 'Y axis step:', (20, 70))
        ztext=wx.StaticText(self, -1, 'Z axis step:', (20, 120))
        self.sc1 = wx.SpinCtrl(self, -1, str(w), (100, 65), (60, -1), min=-20, max=20)
        self.sc2 = wx.SpinCtrl(self, -1, str(h), (120, 65), (60, -1), min=-20, max=20)
        bot_h_sizer.Add(ytext)
        bot_h_sizer.Add(self.sc1)
        bot_h_sizer.Add(ztext)
        bot_h_sizer.Add(self.sc2)

    def plotting(self):
        plotcoordsx = []
        plotcoordsy = []
        for coord in mcn.circcoords:
            plotcoordsx.append(coord[0])
            plotcoordsy.append(coord[1])
        self.axes.clear()
        self.axes.plot(plotcoordsx, plotcoordsy)
        self.figure.set_canvas(self.figurecanvas)
        self.figurecanvas.draw()

    #Button Events

    def OnButton(self,event):
        global frame
        buttonLabel =  event.GetEventObject().GetLabel()
        if buttonLabel == "Open csv File":
            dlg = wx.FileDialog(self, "Open CSV File", wildcard="*.csv")
            result = dlg.ShowModal()
            if result == wx.ID_OK:
                mcn.openFileM(dlg.Path)
            dlg.Destroy()
            self.GetParent().SetStatusText('Opening cvs file')

        elif buttonLabel == "Cut":
            result = wx.MessageBox("Before said YES, be sure that eveything is connected and these are the correct parameters",
                                   "Are you sure you want to cut these file?",
                                   wx.YES_NO|wx.CENTER|wx.ICON_WARNING)
            if result == wx.NO:
                pass
            else:
                print ("Cutting Coords")
                mcn.move_Motors(mcn.circcoords)
                self.GetParent().SetStatusText('Cutting File')

        elif buttonLabel == "Help":
            print ("Help")
            frame = helpFrame(None, "Simple HTML Browser")
            frame.Show()
            self.GetParent().SetStatusText('How to use the foam cutter')

        elif buttonLabel == "Plot":
            print ("Plotting")
            self.GetParent().SetStatusText('Plotting coordinates')
            #plottear
            self.plotting()
            '''
            plotcoordsx = []
            plotcoordsy = []
            for coord in mcn.circcoords:
                plotcoordsx.append(coord[0])
                plotcoordsy.append(coord[1])
            self.axes.clear()
            self.axes.plot(plotcoordsx, plotcoordsy)
            self.figure.set_canvas(self.figurecanvas)
            self.figurecanvas.draw()'''

        elif buttonLabel == "Open dat File":
            dlg = wx.FileDialog(self, "Open airfoil dat File", wildcard="*.dat")
            result = dlg.ShowModal()
            if result == wx.ID_OK:
                mcn.OpenDatFile(dlg.Path)
            dlg.Destroy()


            print("Opening dat file")

        elif buttonLabel == "Move Axis":
            self.GetParent().SetStatusText('Moving axis')
            mcn.move_step(self.sc2.GetValue(),self.sc1.GetValue(),self.sc1.GetValue(),0)

        elif buttonLabel == "About":
            print ("About")
            self.GetParent().SetStatusText('Know about us')
            self.GetParent().OnAbout(wx.ID_ABOUT)

        elif buttonLabel == "Scale":
            self.GetParent().SetStatusText('Press enter in the text box')


    def OnEnterPressed(self,event):
        mcn.circcoords
        scale = float(event.GetEventObject().GetValue())
        for coord in mcn.circcoords:
            for i in range (0,3):
                coord[i]*=scale

        print("scaling to:", scale)
        self.plotting()

class MyFrame(wx.Frame):
    def __init__(self, parent, title=""):
        super(MyFrame, self).__init__(parent, title=title, size=(650,650))

        # Set the panel
        self.panel = MyPanel(self)
        self.SetBackgroundColour( wx.Colour( 0, 141, 254 ) )
        # Setup Menus
        menubar = wx.MenuBar()
        filemenu = wx.Menu()
        filemenu.Append(wx.ID_OPEN)
        filemenu.Append(wx.ID_CUT)
        menubar.Append(filemenu, "File")
        configmenu = wx.Menu()
        configmenu.Append(wx.ID_SETUP, "OSC Server On")
        configmenu.Append(wx.ID_ABOUT, "About Mcnulty")
        menubar.Append(configmenu, "Setup")
        self.SetMenuBar(menubar)


        # Setup StatusBar
        self.CreateStatusBar()
        self.PushStatusText("Run the Machine configuration")


        # Event Handlers
        self.Bind(wx.EVT_MENU, self.OnAbout, id=wx.ID_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnCut, id=wx.ID_CUT)
        self.Bind(wx.EVT_MENU, self.OnOpen, id=wx.ID_OPEN)
        self.Bind(wx.EVT_MENU, self.OnOSC, id=wx.ID_SETUP)



    def OnOpen(self, event):
        dlg = wx.FileDialog(self, "Open CSV File", wildcard="*.csv")
        result = dlg.ShowModal()
        if result == wx.ID_OK:
            mcn.openFileM(dlg.Path)
        dlg.Destroy()
        #self.SetStatusText("Open file: %s" % self._file)

    def OnCut(self, event):
        mcn.move_Motors(mcn.circcoords)
        self.SetStatusText("Cutting File")

    def OnOSC(self, event):

        mcn.initOSC(self)
        self.SetStatusText("OSC Setup")



    #About menu
    def OnAbout(self, event):
        """Show the about dialog"""
        info = wx.AboutDialogInfo()

        # Make a template for the description
        desc = ["\nFoam Cutter 2016\n",
                "Platform Info: (%s,%s)",
                "License: Public Domain"]
        desc = "\n".join(desc)



        # Populate with information
        info.SetName("Mcnulty - Foldable Hot Wire Cutter")
        info.SetVersion("FA16 - 0.8")
        info.SetIcon(wx.Icon('imgs/mcnulty_ico.png'))
        info.SetCopyright("Copyleft : Luis Diaz, Borja Lanza, Marta Verde\nPaco Gonzalez, Maria Santisteban, Jose Real, Victoria Retana")
        info.SetWebSite("http://archive.fabacademy.org/archives/2016/fablabmadridceu/mtm/mtm2016.html")
        info.SetDescription("Hot Wire Foam Cutter GUI System")

        # Create and show the dialog
        wx.AboutBox(info)


class MySplashScreen(wx.SplashScreen):
    """
Create a splash screen widget.
    """
    def __init__(self, parent=None):
        # This is a recipe to a the screen.
        # Modify the following variables as necessary.
        aBitmap = wx.Image(name = "fablogo.png").ConvertToBitmap()
        splashStyle = wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT
        splashDuration = 1000 # milliseconds
        # Call the constructor with the above arguments in exactly the
        # following order.
        wx.SplashScreen.__init__(self, aBitmap, splashStyle,
                                 splashDuration, parent)
        self.Bind(wx.EVT_CLOSE, self.OnExit)

        wx.Yield()
    #----------------------------------------------------------------------#

    def OnExit(self, evt):
        self.Hide()
        # MyFrame is the main frame.
        self.frame = MyFrame(None, title="McNulty Hot Wire Foam Cutter")
        self.frame.SetStatusText("Fab Academy 2016 - From Zero to Hero - MTM Assigment (FabLabs: Leon, Deusto, CEU, UE)")
        self.frame.Show(True);
        # The program will freeze without this line.
        evt.Skip()  # Make sure the default handler runs too...

#Frame with HTML preview for displaying the Help
class helpFrame(wx.Frame):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, -1, title, size=(800,600))
        self.CreateStatusBar()

        html = wx.html.HtmlWindow(self)
        if "gtk2" in wx.PlatformInfo:
            html.SetStandardFonts()
        html.SetRelatedFrame(self, self.GetTitle() + " -- %s")
        html.SetRelatedStatusBar(0)

        wx.CallAfter(
            html.LoadPage, "help.html")




class MyApp(wx.App):
    def OnInit(self):
        #machineSetup()
        MySplash = MySplashScreen()
        MySplash.Show()


        #self.frame = MyFrame(None, title="McNulty Hot Wire Foam Cutter")
        #self.frame.SetStatusText("Fab Academy 2016 - From Zero to Hero - MTM Assigment (FabLabs: Leon, Deusto, CEU, UE)")
        #self.frame.Show();
        return True

