from xml.etree import ElementTree

varTexto = ""

with open('config.xml', 'rt') as f:
    tree = ElementTree.parse(f)

for node in tree.findall('.//config'):
    portname = node.attrib.get('portName')
    if portname:
        varTexto = portname

print "La variable es " + varTexto