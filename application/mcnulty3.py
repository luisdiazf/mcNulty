# Based in the  Two stage example Virtual Machine file example by  Nadya Peek Dec 2014
# Inspired by MTM 2015 foam cutters machines of Puebla (Mexico), Amsterdam, Singapore

# MTM 2016 - FabAcademy 2016
# Supernode FabLab Leon (Leon, Deusto, Universidad Europea, Madrid CEU)
# Creative Coding : Luis Diaz, Borja Lanza, Marta Verde
# Paco Gonzalez, Jose Real, Victoria Retana, Maria Santisteban

#For Review information : Luis (Graphs and data files imports + bug fixing), Borja (GUI design), Marta (OSC, code cleaning + bug fixing) Check personal pages for more info.

#------IMPORTS-------
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#------Pygestalt-------
from pygestalt import nodes
from pygestalt import interfaces
from pygestalt import machines
from pygestalt import functions
from pygestalt.machines import elements
from pygestalt.machines import kinematics
from pygestalt.machines import state
from pygestalt.utilities import notice
from pygestalt.publish import rpc       #remote procedure call dispatcher

#------Other Libraries-------
import time
import io
import sys
import csv
import threading
import OSC #pyOSC
from StringIO import StringIO

#Very important now all the GUI is in another Python file named mcnulty_gui since v3. You need both files!!!!
import mcnulty_gui as GUI



#------Other Python scripts-------
import pdb

#matplotlib config
import matplotlib as mpl
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.figure import Figure
#mpl.rcParams['axes.facecolor'] = '111111' # Graph background color
mpl.rcParams['savefig.transparent'] = True # Graph background color

#Global Variables
circcoords = []
(w, h) = (2, 2)

#------VIRTUAL MACHINE------
class virtualMachine(machines.virtualMachine):

    def initInterfaces(self):
        if self.providedInterface: self.fabnet = self.providedInterface         #providedInterface is defined in the virtualMachine class.
        #else: self.fabnet = interfaces.gestaltInterface('FABNET', interfaces.serialInterface(baudRate = 115200, interfaceType = 'ftdi', portName = 'COM6')) # Uncomment for Windows
        else: self.fabnet = interfaces.gestaltInterface('FABNET', interfaces.serialInterface(baudRate = 115200, interfaceType = 'ftdi', portName = '/dev/tty.usbserial-FTXW9XTL'))#Route for Mac (serial may change)

    def initControllers(self):
        self.yAxisNode = nodes.networkedGestaltNode('Y Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.zlAxisNode = nodes.networkedGestaltNode('ZL Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.zrAxisNode = nodes.networkedGestaltNode('ZR Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.wAxisNode = nodes.networkedGestaltNode('W Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
        self.yzzwNode = nodes.compoundNode(self.yAxisNode, self.zlAxisNode, self.zrAxisNode, self.wAxisNode)

    def initCoordinates(self):
        self.position = state.coordinate(['mm', 'mm', 'mm', 'rev'])

    def initKinematics(self):
        self.yAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(False)])
        self.zlAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(False)])
        self.zrAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(False)])
        self.wAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8),  elements.invert.forward(False)])

        self.stageKinematics = kinematics.direct(4)     #direct drive on all axes

    def initFunctions(self):
        self.move = functions.move(virtualMachine = self, virtualNode = self.yzzwNode, axes = [self.yAxis, self.zlAxis, self.zrAxis, self.wAxis], kinematics = self.stageKinematics, machinePosition = self.position,planner = 'null')
        self.jog = functions.jog(self.move)     #an incremental wrapper for the move function
        pass

    def initLast(self):
        #self.machineControl.setMotorCurrents(aCurrent = 0.8, bCurrent = 0.8, cCurrent = 0.8)
        #self.xNode.setVelocityRequest(0)       #clear velocity on nodes. Eventually this will be put in the motion planner on initialization to match state.
        pass

    def publish(self):
        #self.publisher.addNodes(self.machineControl)
        pass

    def getPosition(self):
        return {'position':self.position.future()}

    def setPosition(self, position  = [None]):
        self.position.future.set(position)

    def setSpindleSpeed(self, speedFraction):
        #self.machineControl.pwmRequest(speedFraction)
        pass



#---- OSC

def initOSC(self):
    receive_address = '192.168.0.11', 8000
    global s
    # OSC Server. there are three different types of server.
    s = OSC.OSCServer(receive_address) # basic

    # this registers a 'default' handler (for unmatched messages),
    # an /'error' handler, an '/info' handler.
    # And, if the client supports it, a '/subscribe' & '/unsubscribe' handler
    s.addDefaultHandlers()

    s.addMsgHandler("MTM/btn_help/", button_handler) # adding our function
    s.addMsgHandler("MTM/btn_move/", button_handler) # adding our function

# Start OSCServer
    print "\nStarting OSCServer. You will need to force quit for kill "
    global st
    st = threading.Thread( target = s.serve_forever )
    st.start()


    # OSC address handlers
def button_handler(addr, tags, args, source):

    if (args[0]==1.0 and addr == "/MTM/btn_move"):
        #print "Bang"
        #openFile()
        mcn.move_Motors(circcoords)
    elif (args[0]==1.0 and addr == "/MTM/btn_help"):
        print "Bang"
        frame = self.helpFrame()
        frame.Show()
    else:
        pass

def null_handler(addr, tags, args, source):
    pass


#Funtions


def openFile():
    circcoords[:] = []
    fname = raw_input("Enter filename: ")
    if len(fname) == 0:
        fname = "test2.csv"
    try:
        fh = open(fname,'r')
    except:
        print "Error: File (", fname, ") cannot be opened. \nOpening test file instead."
        fname = "test2.csv"
        fh = open(fname,'r')
        #exit()


    for line in fh.readlines():
        xy = line.split(',')
        coord = []
        for num in xy:
            try:
                nr = int(num)
                coord.append(nr)
            except:
                pass
        circcoords.append(coord)
    circcoords.append([0,0,0,0])
    fh.close()

    #move_Motors(circcoords)
    print circcoords
    print ("Reading done!")

def openFileM(object):
    circcoords[:] = []
    fh = open(object,'r')
    #circcoords = []
    for line in fh.readlines():
        xy = line.split(',')
        coord = []
        for num in xy:
            try:
                nr = int(num)
                coord.append(nr)
            except:
                pass
        circcoords.append(coord)
    circcoords.append([0,0,0,0])
    fh.close()

    #move_Motors(circcoords)
    print circcoords
    print ("Reading done!")

#Function for open dat files
def OpenDatFile(object):
    circcoords[:] = []
    fh = open(object,'r')
    #circcoords = []
    for line in fh.readlines():
        xy = line.split()
        coord = []
        if (xy[0] != 'NACA') & (xy[0] != '100.00') & (xy[1] != '......'):
            for num in xy:
                try:
                    num=num.strip('(')
                    num=num.strip(')')
                    nr = float(num)
                    coord.append(nr)
                except:
                    pass
            coord.append(0)   
            coord.append(0)    
            circcoords.append(coord)
    #circcoords.append([0,0,0,0])
    OffsetDat(circcoords)
    fh.close()

    #move_Motors(circcoords)
    print circcoords
    print ("Reading dat file done!")

#offset function so every z coord is positive
def OffsetDat(datCoords):
    minz = 10
    for coord in datCoords:
        if coord[1]<minz:
            minz=coord[1]
    for coord in datCoords:
        coord[1]+=abs(minz)
        coord[1]=round(coord[1],4)
        coord[2]=coord[1]

 #Main function for moving the motors
def move_Motors(moves):

    for element in moves:
        print element


    print ("Cut Done")
    '''
    for move in moves:
        stages.move(move, 0)
        status = stages.yAxisNode.spinStatusRequest()
        # This checks to see if the move is done.
        while status['stepsRemaining'] > 0:
            time.sleep(0.001)
            status = stages.yAxisNode.spinStatusRequest()
    '''

#Function for moving step motors a litte steps for fine adjusting
def move_step(a,b,c,d):
    print (a,b)
    #Comment for running application without Gestalt Nodes
    #stages.jog([a, b, c, d])




def machineSetup():
    # The persistence file remembers the node you set. It'll generate the first time you run the
    # file. If you are hooking up a new node, delete the previous persistence file.
    stages = virtualMachine(persistenceFile = "test.vmp")

    # You can load a new program onto the nodes if you are so inclined. This is currently set to
    # the path to the 086-005 repository on Nadya's machine.
    #stages.xyNode.loadProgram('../../../086-005/086-005a.hex')


    # This is a widget for setting the potentiometer to set the motor current limit on the nodes.
    # The A4982 has max 2A of current, running the widget will interactively help you set.
    #stages.xyNode.setMotorCurrent(0.7)

    # This is for how fast the motors for move
    stages.yzzwNode.setVelocityRequest(1)



if __name__ == "__main__":
    #machineSetup()
    app = GUI.MyApp(False)
    app.MainLoop()